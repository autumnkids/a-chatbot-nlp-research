package edu.usc.ict.nl.nlu.directablechar.action;

import java.util.List;

public class Query extends Action {
	public Query(List<String> args) {
		this.name = "QUERY";
		this.arguments = args;
	}
	
	@Override
	public boolean good() {
		return true;
	}
}
