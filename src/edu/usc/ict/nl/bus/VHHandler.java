package edu.usc.ict.nl.bus;

import java.io.File;
import java.util.Map;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import edu.usc.ict.nl.audio.util.Audio;
import edu.usc.ict.nl.bus.events.DMSpeakEvent;
import edu.usc.ict.nl.bus.events.NLUEvent;
import edu.usc.ict.nl.bus.events.SystemUtteranceDoneEvent;
import edu.usc.ict.nl.bus.events.SystemUtteranceLengthEvent;
import edu.usc.ict.nl.bus.modules.DM;
import edu.usc.ict.nl.bus.modules.NLGInterface;
import edu.usc.ict.nl.nlu.NLUOutput;
import edu.usc.ict.nl.vhmsg.VHBridge;
import edu.usc.ict.nl.vhmsg.VHBridge.VRGenerate;
import edu.usc.ict.nl.vhmsg.VHBridge.VRNLU;
import edu.usc.ict.nl.vhmsg.VHBridge.VRPlaySound;
import edu.usc.ict.nl.vhmsg.VHBridge.VRSpoke;
import edu.usc.ict.nl.vhmsg.VHBridgewithMinat;
import edu.usc.ict.vhmsg.MessageEvent;
import edu.usc.ict.vhmsg.MessageListener;

public abstract class VHHandler extends NLBusBase {
	
	private VHBridgewithMinat vhBridge=null;
	public VHBridgewithMinat getVHBridge() {return vhBridge;}
	protected void setVHBridge(VHBridgewithMinat s) {this.vhBridge=s;}
	protected String vhMyself,vhOther;
	protected boolean inTwoVHCharactersMode=false,usingJustVRSpeak=false;
	private long sent=0;
	
	protected XStream nluOutputCenverter=new XStream(new StaxDriver());

	public VHHandler() throws Exception {
		super();
	}
	
	//vrSpoke Brad all 1370985939725-33-1 Our brains are controlled by the NPC Editor.


	protected MessageListener createVrSpeechMessageListener() {
		return new MessageListener() {

			public void messageAction(MessageEvent e)
			{
				VHBridge.VRSpeech msg=null;
				try {
					// this will fire an exception if the input message is not a vrExpress message. 
					msg=vhBridge.processVrSpeechEvent(e);
				} catch (Exception ex){
					System.out.println("MessageListener.messageAction received non vrSpeech message.");
				}
				if (msg!=null) {
					if (!inTwoVHCharactersMode || vhMyself==null || vhMyself.equals(msg.getSpeaker())) {
						if (msg.isComplete()) {
							for(Long sessionID : getSessions()) {
								try {
									setSpeakingStateVarForSessionAs(sessionID, false);
									handleTextUtteranceEvent(sessionID, msg.getUtterance());
								} catch (Exception e1) {
									logger.error("Error processing vrSpeech message: ",e1);
								}
							}
						} else if (!usingJustVRSpeak) {
							for(Long sessionID : getSessions()) {
								try {
									setSpeakingStateVarForSessionAs(sessionID, true);
								} catch (Exception e1) {
									logger.error("Error processing vrSpeech message: ",e1);
								}
							}
						}
					}
				}
			}
		};
	}
	protected MessageListener createVrNLUMessageListener() {
		return new MessageListener() {

			public void messageAction(MessageEvent e)
			{
				VRNLU msg=null;
				try {
					// this will fire an exception if the input message is not a vrExpress message. 
					msg=vhBridge.processVrNLUEvent(e);
				} catch (Exception ex){
					//System.out.println("MessageListener.messageAction received non vrExpress message.");
				}
				if (!inTwoVHCharactersMode || vhMyself==null || vhMyself.equals(msg.getSpeaker())) {
					if (msg!=null) {
						if (msg.isComplete()) {
							for(Long sessionID : getSessions()) {
								try {
									setSpeakingStateVarForSessionAs(sessionID, false);
									String interpretation=msg.getInterpretation();
									//NLUOutput nluOutput=NLUOutput.fromObjectDump(interpretation);
									//NLUOutput nluOutput=new NLUOutput(null, ), 1, null);
									NLUOutput nluOutput=(NLUOutput) nluOutputCenverter.fromXML(interpretation);
									handleNLUEvent(sessionID, new NLUEvent(nluOutput,sessionID));
								} catch (Exception e1) {
									logger.error("Error processing vrNLU event: ",e1);
								}
							}
						} else {
							for(Long sessionID : getSessions()) {
								try {
									setSpeakingStateVarForSessionAs(sessionID, true);
								} catch (Exception e1) {
									logger.error("Error processing vrNLU event: ",e1);
								}
							}
						}
					}
				}
			}
		};
	}
	protected MessageListener createVrPerceptionMessageListener() {
		return new MessageListener() {

			public void messageAction(MessageEvent e)
			{
				VHBridge.VRPerception msg=null;
				try {
					// this will fire an exception if the input message is not a vrExpress message. 
					msg=vhBridge.processVrPerceptionEvent(e);
				} catch (Exception ex){
					//logger.warn("MessageListener.messageAction received non vrPerception message.");
				}
				if (msg!=null) {
					try {
						handlePerceptionEvent(msg);
					} catch (Exception e1) {
						logger.error("Error processing PML message:",e1);
					}
				}
			}
		};
	}
	protected MessageListener createMinatMessageListener() {
		return new MessageListener() {

			public void messageAction(MessageEvent e)
			{
				VHBridgewithMinat.Minat msg=null;
				try {
					// this will fire an exception if the input message is not a vrExpress message. 
					msg=vhBridge.processMinatEvent(e);
				} catch (Exception ex){
					//logger.warn("MessageListener.messageAction received non vrPerception message.");
				}
				if (msg!=null) {
					try {
						handleMinatEvent(msg);
					} catch (Exception e1) {
						logger.error("Error processing PML message:",e1);
					}
				}
			}
		};
	}
	protected MessageListener createVrGenerateMessageListener() {
		return new MessageListener() {

			public void messageAction(MessageEvent e)
			{
				VHBridge.VRGenerate msg=null;
				try {
					// this will fire an exception if the input message is not a vrExpress message. 
					msg=vhBridge.processVrGenerateEvent(e);
				} catch (Exception ex){
					//logger.warn("MessageListener.messageAction received non vrPerception message.");
				}
				if (msg!=null) {
					if (!inTwoVHCharactersMode || vhMyself==null || vhMyself.equals(msg.getAgent())) {
						try {
							handleDMSpeakEvent(msg);
						} catch (Exception e1) {
							logger.error("Error processing PML message:",e1);
						}
					}
				}
			}
		};
	}
	
	protected MessageListener createVrPlaySoundMessageListener() {
		return new MessageListener() {

			public void messageAction(MessageEvent e)
			{
				VHBridge.VRPlaySound msg=null;
				try {
					// this will fire an exception if the input message is not a vrExpress message. 
					msg=vhBridge.processVrPlaySoundEvent(e);
				} catch (Exception ex){
					//logger.warn("Error processing a VHBridge.VRPlaySound event.",ex);
				}
				if (msg!=null) {
					if (!inTwoVHCharactersMode || vhMyself==null || vhMyself.equals(msg.getAgent())) {
						try {
							handleVRPlaySoundEvent(msg);
						} catch (Exception e1) {
							logger.error("Error processing VRPlaySound message:",e1);
						}
					} else if (inTwoVHCharactersMode && !vhMyself.equals(msg.getAgent()) && !usingJustVRSpeak) {
						for(Long sessionID : getSessions()) {
							try {
								setSpeakingStateVarForSessionAs(sessionID, true);
							} catch (Exception e1) {
								logger.error("Error processing VRPlaySound message:",e1);
							}
						}
					}
				}
			}
		};
	}
	protected MessageListener createVrSpokeMessageListener() {
		return new MessageListener() {

			public void messageAction(MessageEvent e)
			{
				VHBridge.VRSpoke msg=null;
				try {
					// this will fire an exception if the input message is not a vrExpress message. 
					msg=vhBridge.processVrSpokeEvent(e);
				} catch (Exception ex){
					//logger.warn("MessageListener.messageAction received non vrPerception message.");
				}
				if (msg!=null) {
					if (!inTwoVHCharactersMode || vhMyself==null || vhMyself.equals(msg.getSpeaker())) {
						try {
							handleVRSpokeEvent(msg);
						} catch (Exception e1) {
							logger.error("Error processing VRSpoke message:",e1);
						}
						if (inTwoVHCharactersMode && !usingJustVRSpeak) {
							vhBridge.sendVRSpeech(msg.getText(), vhOther, sent++);
						}
					} else if (inTwoVHCharactersMode && !vhMyself.equals(msg.getSpeaker()) && !usingJustVRSpeak) {
						for(Long sessionID : getSessions()) {
							try {
								setSpeakingStateVarForSessionAs(sessionID, false);
								handleTextUtteranceEvent(sessionID, msg.getText());
							} catch (Exception e1) {
								logger.error("Error processing vrSpoke event from "+vhOther+" into an utterance event for myself:",e1);
							}
						}
					}
				}
			}
		};
	}
	protected MessageListener createVrSpeakMessageListener() {
		return new MessageListener() {

			public void messageAction(MessageEvent e)
			{
				VHBridge.VRSpeak msg=null;
				try {
					// this will fire an exception if the input message is not a vrExpress message. 
					msg=vhBridge.processVrSpeakEvent(e);
				} catch (Exception ex){
					//logger.warn("MessageListener.messageAction received non vrPerception message.");
				}
				if (msg!=null) {
					if (inTwoVHCharactersMode && usingJustVRSpeak) {
						if (vhMyself==null || vhMyself.equals(msg.getSpeaker())) {
							vhBridge.sendVRSpeech(msg.getText(), vhOther, sent++);
						} else {
							for(Long sessionID : getSessions()) {
								try {
									handleTextUtteranceEvent(sessionID, msg.getText());
								} catch (Exception e1) {
									logger.error("Error processing vrSpoke event from "+vhOther+" into an utterance event for myself:",e1);
								}
							}
						}
					}
				}
			}
		};
	}
	protected MessageListener createVrLauncherMessagesListener() {
		return new MessageListener() {
			public void messageAction(MessageEvent e) {
				String componentName=getConfiguration().getVhComponentId();
				Map<String, ?> map = e.getMap();
				if (map.containsKey("vrAllCall")) {
					vhBridge.sendComponetIsAlive(componentName);
				} else if (map.containsKey("vrKillComponent")) {
					String msg=(String) map.get("vrKillComponent");
					if (msg.equals(componentName) || msg.equalsIgnoreCase("all")) {
						vhBridge.sendComponentKilled(componentName);
						System.exit(0);
					}
				}
			}
		};
	}
	private void handleDMSpeakEvent(VRGenerate msg) throws Exception {
		for(Long sessionID : getSessions()) {
			if (getCharacterName4Session(sessionID)!=null) {
				DM dm=getPolicyDMForSession(sessionID,false);
				if (dm!=null) {
					DMSpeakEvent event=new DMSpeakEvent(null, msg.getRequest(), sessionID, null, dm.getInformationState());
					handleDMResponseEvent(event);
				}
			}
		}
	}

	private void handleVRSpokeEvent(VRSpoke msg) throws Exception {
		for(Long sessionID : getSessions()) {
			NLGInterface nlg = getNlg(sessionID,false);
			DM dm=getPolicyDMForSession(sessionID, false);
			if (dm!=null && !dm.isSessionDone() && nlg!=null && (nlg instanceof VRSpeakSpokeTrackerInterface)) {
				String sa=((VRSpeakSpokeTrackerInterface)nlg).getSpeechActIDFromVRMessageID(msg.getID());
				if (sa!=null) { 
					dm.handleEvent(new SystemUtteranceDoneEvent(sa, sessionID));
				}
			}
		}
	}
	
	private void handleVRPlaySoundEvent(VRPlaySound msg) throws Exception {
		File f=new File(msg.getFileName());
		float length=Audio.getWavLength(f);
		for(Long sessionID : getSessions()) {
			DM dm=getPolicyDMForSession(sessionID, false);
			if (dm!=null && !dm.isSessionDone()) {
				dm.handleEvent(new SystemUtteranceLengthEvent(f.getName(), sessionID, length));
			}
		}
	}
}
