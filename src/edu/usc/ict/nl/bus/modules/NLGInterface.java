package edu.usc.ict.nl.bus.modules;

import edu.usc.ict.nl.bus.NLBusInterface;
import edu.usc.ict.nl.bus.events.DMInterruptionRequest;
import edu.usc.ict.nl.bus.events.DMSpeakEvent;
import edu.usc.ict.nl.bus.events.NLGEvent;
import edu.usc.ict.nl.kb.DialogueKBInterface;

public interface NLGInterface {
	public NLGEvent doNLG(Long sessionID, DMSpeakEvent ev,boolean simulate) throws Exception;
	public DialogueKBInterface getKBForEvent(DMSpeakEvent ev) throws Exception;
	public void setNLModule(NLBusInterface nlModule);
	public Float getDurationOfThisDMEvent(Long sessionID, NLGEvent ev) throws Exception;
	public boolean isResource(Long sessionID, NLGEvent ev) throws Exception;
	public String getAudioFileName4SA(String sa) throws Exception;
	public void interrupt(DMInterruptionRequest ev) throws Exception;
}
