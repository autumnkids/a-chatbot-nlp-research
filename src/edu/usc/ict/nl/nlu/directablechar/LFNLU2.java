package edu.usc.ict.nl.nlu.directablechar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import edu.usc.ict.nl.bus.modules.NLU;
import edu.usc.ict.nl.config.NLUConfig;
import edu.usc.ict.nl.nlu.NLUOutput;
import edu.usc.ict.nl.nlu.directablechar.BuildTrainingData.LANG;
import edu.usc.ict.nl.util.FileUtils;
import edu.usc.ict.nl.util.StringUtils;

public class LFNLU2 extends NLU {

	private LFNLU nlu=null;
	
	public LFNLU2(NLUConfig c) throws Exception {
		super(c);
		nlu = new LFNLU("http://colo-vm19.isi.edu:8082/",LANG.EN,"KBs/kb.da");
	}

	@Override
	public void kill() throws Exception {
		nlu.kill();
	}
	
	@Override
	public List<NLUOutput> getNLUOutput(String text,Set<String> possibleNLUOutputIDs, Integer nBest) throws Exception {
		NLUConfig config = getConfiguration();
		
		StringBuffer resolverAxioms=null;
		try {
			resolverAxioms=nlu.getObjectKB().generateAxioms();
		} catch (NullPointerException e) {}
		File baseKb=new File(getConfiguration().getNLUContentRoot(),"kb.txt");
		StringBuffer kb=FileUtils.readFromFile(baseKb.getAbsolutePath());
		if (resolverAxioms!=null) kb.append(resolverAxioms);
		System.out.println("---------------------GetNLUOutput--------------------");
		System.out.println(text);
		System.out.println("-----------------------------------------------------");
		NLUOutput r = nlu.testNLU(text,kb);
		
		List<NLUOutput> result=null;
		if (r!=null) {
			if (result==null) result=new ArrayList<NLUOutput>();
			result.add(r);
		} else {
			String lowConfidenceEvent=config.getLowConfidenceEvent();
			if (StringUtils.isEmptyString(lowConfidenceEvent)) {
				logger.warn(" no user speech acts left and LOW confidence event disabled, returning no NLU results.");
			} else {
				if (result==null) result=new ArrayList<NLUOutput>();
				result.add(new NLUOutput(text,lowConfidenceEvent,1f,null));
				logger.warn(" no user speech acts left. adding the low confidence event.");
			}
		}
		return result;
	}
	
	@Override
	public Set<String> getAllSimplifiedPossibleOutputs() throws Exception {
		return null;
	}

	public ObjectKB getObjectKB() {
		if (nlu!=null) return nlu.getObjectKB();
		return null;
	}
}
