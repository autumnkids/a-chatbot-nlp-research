package edu.usc.ict.nl.dm.reward.trackers;

import edu.usc.ict.nl.dm.reward.RewardDM;


public class SystemFinishedSpeakingTracker extends ValueTracker {
	public SystemFinishedSpeakingTracker(RewardDM dm) {
		super(dm);
	}

	@Override
	public Boolean getter() {
		boolean ret=!dm.getSpeakingTracker().isSpeaking();
		setter(ret);
		return ret;
	}

	@Override
	protected void setter(Object cv) {
		boolean currentValue=(Boolean) cv;
		Boolean oldValue=(Boolean)value;
		if (oldValue==null || currentValue!=oldValue) {
			value=currentValue;
			if (currentValue) touch();
		}
	}
}
