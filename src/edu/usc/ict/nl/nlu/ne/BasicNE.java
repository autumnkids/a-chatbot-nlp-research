package edu.usc.ict.nl.nlu.ne;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import edu.usc.ict.nl.bus.modules.NLU;
import edu.usc.ict.nl.bus.special_variables.SpecialEntitiesRepository;
import edu.usc.ict.nl.bus.special_variables.SpecialVar;
import edu.usc.ict.nl.config.NLUConfig;
import edu.usc.ict.nl.nlu.BuildTrainingData;
import edu.usc.ict.nl.nlu.Token;
import edu.usc.ict.nl.parser.ChartParser;
import edu.usc.ict.nl.parser.ChartParser.Item;
import edu.usc.ict.nl.parser.semantics.ParserSemanticRulesTimeAndNumbers;

public abstract class BasicNE implements NamedEntityExtractorI {
	protected SpecialEntitiesRepository svs=new edu.usc.ict.nl.bus.special_variables.SpecialEntitiesRepository();
	private NLUConfig configuration;
	
	protected static final Logger logger = Logger.getLogger(NLU.class.getName());
	static {
		URL log4Jresource = ClassLoader.getSystemResource("log4j.properties");
		if (log4Jresource != null)
			PropertyConfigurator.configure( log4Jresource );
	}
	
	public NLUConfig getConfiguration() {
		return configuration;
	}
	public void setConfiguration(NLUConfig configuration) {
		this.configuration = configuration;
	}
	
	protected void addSpecialVarToRepository(SpecialVar v) {
		svs.addSpecialVariable(v);
	}
	
	
	public static Double extractDouble(String t) {
		Pattern p=Pattern.compile("([\\+\\-]*[\\d\\.]+[eE\\+\\-\\d]*)");
		
		Matcher m=p.matcher(t);
		try {
			if (m.find()) {
				Double number=Double.parseDouble(m.group(1));
				return number;
			}
		} catch (NumberFormatException e) {}
		return null;
	}
	public Double getFirstNumber(List<Token> tokens) {
		ChartParser parser;
		try {
			NLUConfig nluConfig = getConfiguration();
			File grammar=new File(nluConfig.getNLUContentRoot(),"time-period-grammar.txt");
			if (nluConfig.nlBusConfig!=null && !grammar.exists()) grammar=new File(nluConfig.nlBusConfig.getContentRoot(),"common/nlu/time-period-grammar.txt");
			if (!grammar.exists()) return null;
			else {
				parser = ChartParser.getParserForGrammar(grammar);
				ArrayList<String> input=new ArrayList<String>();
				for (Token t:tokens) {
					input.add(t.getName());
				}
				Collection<Item> result = parser.parseAndFilter(input,"<NUM>");
				if (result.isEmpty()) return null;
				else {
					Iterator<Item> resultIterator = result.iterator();
					Item r=resultIterator.next();
					return (Double)r.getSemantics();
				}
			}
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	public static Long getTimePeriodInSeconds(List<Token> tokens) {
		ChartParser parser;
		try {
			parser = ChartParser.getParserForGrammar(new File("resources/characters/common/nlu/time-period-grammar.txt"));
			ArrayList<String> input=new ArrayList<String>();
			for (Token t:tokens) {
				input.add(t.getName());
			}
			Collection<Item> result = parser.parseAndFilter(input,"<TP>");
			if (result.size()==1) {
				Iterator<Item> resultIterator = result.iterator();
				Item r=resultIterator.next();
				return Math.round((Double)r.getSemantics());
			}
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	public static Double convertSecondsIn(Long seconds,Double conversion) {		
		if (seconds!=null) {
			return seconds/conversion;
		}
		return null;
	}
	public static boolean equalDoublesWithPrecision(Double d1,Double d2,double precision) {
		precision=Math.abs(precision);
		if ((d1==null) || (d2==null)) return d1==d2;
		else return ((d1<=(d2+Math.abs(d2)*precision)) && (d1>=(d2-(Math.abs(d2)*precision))));
	}
	
	// return number of times per day
	public static Double getTimesEachDay(List<Token> tokens) {
		ChartParser parser;
		try {
			parser = ChartParser.getParserForGrammar(new File("resources/characters/common/nlu/time-period-grammar.txt"));
			ArrayList<String> input=new ArrayList<String>();
			for (Token t:tokens) {
				input.add(t.getName());
			}
			Collection<Item> result = parser.parseAndFilter(input,"<FP>");
			Double prevTimesEachSecond=null;
			for (Item r:result) {
				Double timesEachSecond=(Double)r.getSemantics();
				if ((prevTimesEachSecond!=null) && !equalDoublesWithPrecision(prevTimesEachSecond,timesEachSecond,0.01)) return null;
				else prevTimesEachSecond=timesEachSecond;
			}
			if (prevTimesEachSecond==null) {
				logger.info(" extracting frequency from: "+BuildTrainingData.untokenize(tokens));
				return null;
			} else return ParserSemanticRulesTimeAndNumbers.numSecondsInDay*prevTimesEachSecond;
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}

	@Override
	public Token generalize(Token input) {
		return null;
	}
}
