package edu.usc.ict.nl.nlu.mallet;

import java.io.File;
import java.util.Iterator;

import cc.mallet.classify.MaxEnt;
import cc.mallet.types.Alphabet;
import cc.mallet.types.FeatureSelection;
import cc.mallet.types.Label;
import cc.mallet.types.LabelAlphabet;
import edu.usc.ict.nl.config.NLUConfig;
import edu.usc.ict.nl.nlu.Model;
import edu.usc.ict.nl.nlu.NLUProcess;
import edu.usc.ict.nl.nlu.jmxnlu.JMXClassifierNLU;

public class MalletMaxEntClassifierNLU extends JMXClassifierNLU {

	public MalletMaxEntClassifierNLU(NLUConfig config) throws Exception {
		super(config);
	}

	@Override
	public NLUProcess startMXNLUProcessWithTheseParams(String model,
			int nbest) throws Exception {
		NLUProcess p = new MalletMaxEntClassifierProcess(null);
		p.run(model, nbest);
		return p;
	}
	
	@Override
	public Model readModelFileNoCache(File mf) throws Exception {
		Model ret=null;

		MalletMaxEntClassifierProcess tmp = new MalletMaxEntClassifierProcess(null);
		MaxEnt classifier=tmp.loadClassifier(mf);


		int defaultFeatureIndex=classifier.getDefaultFeatureIndex();
		int numFeatures = defaultFeatureIndex + 1;

		LabelAlphabet labelAlphabet = classifier.getLabelAlphabet();
		int numLabels = labelAlphabet.size();
		double[] parameters = classifier.getParameters();
		FeatureSelection[] perClassFeatureSelection = classifier.getPerClassFeatureSelection();
		double[] scores=new double[numLabels]; 
		assert (scores.length == numLabels);
		for (int li = 0; li < numLabels; li++) {
			Label label=labelAlphabet.lookupLabel(li);
			FeatureSelection featureSelectionForThisLabel = (perClassFeatureSelection == null ? classifier.getFeatureSelection():perClassFeatureSelection[li]);
			Alphabet featureAlphabet = featureSelectionForThisLabel.getAlphabet();
			Iterator<String> featuresIt=featureAlphabet.iterator();
			while(featuresIt.hasNext()) {
				String feature=featuresIt.next();
				int ri=featureAlphabet.lookupIndex(feature);
				if (ret==null) ret=new Model();
				ret.addFeatureWeightForSA(feature, label.toString(), ret.new FeatureWeight(feature,(float)parameters[li*numFeatures + defaultFeatureIndex], (float)parameters[li*numFeatures+ri]));
			}
		}
		return ret;
	}
}
