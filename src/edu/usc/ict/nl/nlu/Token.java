package edu.usc.ict.nl.nlu;


public class Token {
	public enum TokenTypes {WORD,NUM,OTHER,O1};
	String name,original;
	TokenTypes type;

	public Token(String name, TokenTypes type,String original) {
		setName(name);
		setType(type);
		setOriginal(original);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TokenTypes getType() {
		return type;
	}
	public void setType(TokenTypes type) {
		this.type = type;
	}
	public String getOriginal() {
		return original;
	};
	public void setOriginal(String original) {
		this.original = original;
	}
	
	@Override
	public String toString() {
		return "["+getName()+"("+getOriginal()+"): "+getType()+"]";
	}
}
