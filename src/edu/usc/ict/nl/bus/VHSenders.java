package edu.usc.ict.nl.bus;

import edu.usc.ict.nl.config.NLBusConfig;
import edu.usc.ict.nl.nlu.NLUOutput;
import edu.usc.ict.nl.vhmsg.VHBridge;

public abstract class VHSenders extends VHHandler {
	
	public VHSenders() throws Exception {
		super();
	}

	protected void sendVrNLU(Long sessionId,NLUOutput nluOutput) {
		NLBusConfig config=getConfiguration();
		VHBridge vhBridge=getVHBridge();
		if (vhBridge!=null && config.getNluVhGenerating()) {
			String speaker=config.getVhSpeaker();
			try {
				//String nluSA=nluOutput.toObjectDump();
				String nluSA=nluOutputCenverter.toXML(nluOutput);
				vhBridge.sendVRNLU(nluSA, speaker, sessionId);
			} catch (Exception e) {
				logger.error("exception while dumping NLUOut '"+nluOutput+"' to string.", e);
			}
		}
	}

	protected void sendVrGenerate(Long sessionId,String generateID) {
		NLBusConfig config=getConfiguration();
		VHBridge vhBridge=getVHBridge();
		if (vhBridge!=null && config.getDmVhGenerating()) {
			String speaker=config.getVhSpeaker();
			vhBridge.sendVrGenerate(generateID, speaker, sessionId);
		}
		
	}
	
}
