package edu.usc.ict.nl.nlu.ne;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.usc.ict.nl.bus.special_variables.SpecialVar;
import edu.usc.ict.nl.nlu.BuildTrainingData;
import edu.usc.ict.nl.nlu.Token;

public class Numbers extends BasicNE {
	
	private Pattern[] sas=null;
	
	public static final SpecialVar numVar=new SpecialVar(null,"NUM",
			"Number extracted from a answer.number or answer.number-in-period speech acts.","0",Number.class);
	public Numbers(String... sas) {
		addSpecialVarToRepository(numVar);
		this.sas=new Pattern[sas.length];
		for (int i=0;i<sas.length;i++) {
			this.sas[i]=Pattern.compile(sas[i]);
		}
	}
	
	@Override
	public Map<String, Object> extractPayloadFromText(String text,String speechAct) throws Exception {
		Map<String, Object> payloads = null;
		if (sas!=null) {
			boolean match=false;
			for(int i=0;i<sas.length;i++) {
				Matcher m=sas[i].matcher(speechAct);
				if (match=m.matches()) break;
			}
			if (match) {
				// try to extract the number from the answer and return that as payload
				Double num = extractDouble(text);
				if (num==null) {
					List<Token> tokens = BuildTrainingData.tokenize(text);
					num=getFirstNumber(tokens);
				}
				if (num!=null) {
					logger.info("Extracted number "+num+" from the answer '"+text+"'.");
					if (payloads==null) payloads=new HashMap<String, Object>();
					payloads.put(numVar.getOriginalName(), num);
				}
			}
		}
		return payloads;
	}
}
