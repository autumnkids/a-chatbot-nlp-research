package edu.usc.ict.nl.nlu.fst.train;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.usc.ict.nl.bus.modules.NLU;
import edu.usc.ict.nl.config.NLBusConfig;
import edu.usc.ict.nl.config.NLUConfig;
import edu.usc.ict.nl.nlu.TrainingDataFormat;
import edu.usc.ict.nl.nlu.fst.FSTNLUOutput;
import edu.usc.ict.nl.nlu.fst.sps.test.NLUTest;
import edu.usc.ict.nl.util.Pair;
import edu.usc.ict.nl.util.StringUtils;
import edu.usc.ict.nl.utils.ExcelUtils;

public class TDGeneralizerAndLexiconBuilder {
	
	List<Pattern> patterns=null; 
	/**
	 * provides the list of labels that the training data must comply with.
	 * Any training data point must use a label that contain as prefix one of the valid labels provided.
	 * The suffix part goes into the lexicon,
	 * @param labels
	 */
	public TDGeneralizerAndLexiconBuilder(String... labels) {
		patterns=new ArrayList<Pattern>();
		for(String l:labels) {
			patterns.add(Pattern.compile(l));
		}
	}
	
	public Map<String,Set<String>> generalizeTDAndBuildLexicon(List<TrainingDataFormat> tds) {
		Map<String,Set<String>> lexicon=null;
		if (tds!=null) {
			for(TrainingDataFormat td:tds) {
				String label=td.getLabel();
				List<Pair<String, String>> decomposedLabel = FSTNLUOutput.getPairsFromString(label);
				for(Pair<String, String> kv:decomposedLabel) {
					label=kv.getFirst()+FSTNLUOutput.keyValueSep+kv.getSecond();
					for (Pattern p:patterns) {
						Matcher m=p.matcher(label);
						if (m.matches()) {
							if (m.groupCount()>1) {
								String base=m.group(1);
								String lex=m.group(m.groupCount());
								if (!StringUtils.isEmptyString(lex)) {
									base=base.replaceFirst(FSTNLUOutput.keyValueSep+"$", "");
									td.setLabel(base);
									String lexiconKey=base;
									String[] baseParts=base.split(FSTNLUOutput.keyValueSep);
									if (baseParts.length>1) {
										lexiconKey=baseParts[baseParts.length-1];
										StringBuffer b=new StringBuffer();
										boolean first=true;
										for(int i=0;i<baseParts.length-1;i++) {
											b.append(baseParts[i]+((first?"":FSTNLUOutput.keyValueSep)));
											first=false;
										}
										base=b.toString();
									}
									if (lexicon==null) lexicon=new HashMap<String, Set<String>>();
									Set<String> lexiconForBase=lexicon.get(lexiconKey);
									if (lexiconForBase==null) lexicon.put(lexiconKey, lexiconForBase=new HashSet<String>());
									lexiconForBase.add(lex);
								}
							}
						}
					}
				}
			}
		}
		return lexicon;
	}
	
	public List<TrainingDataFormat> readTDFiles(File... files) {
		List<TrainingDataFormat> itds=null;
		for(File u:files) {
			List<TrainingDataFormat> tds=null;
			try {
				tds = Aligner.extractTrainingDataFromSingleStep1and3GoogleXLSXForSPS(u);
				if (tds!=null) {
					if (itds==null) itds=new ArrayList<TrainingDataFormat>();
					itds.addAll(tds);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return itds;
	}
	
	private Map<String, Set<String>> buildActualLexicon(Map<String, Map<String, Integer>> phrases,Map<String, Set<String>> lexicon) {
		Map<String,Set<String>> ret=null;
		if (phrases!=null && lexicon!=null) {
			for(String concept:phrases.keySet()) {
				Map<String, Integer> entriesForconcept = phrases.get(concept);
				if (entriesForconcept!=null && !entriesForconcept.isEmpty()) {
					List<Pair<String, String>> pairs = FSTNLUOutput.getPairsFromString(concept);
					for(Pair<String, String> kv:pairs) {
						String label=kv.getFirst()+FSTNLUOutput.keyValueSep+kv.getSecond();
						String[] baseParts=label.split(FSTNLUOutput.keyValueSep);
						if (baseParts.length>1) {
							String oneButLast=baseParts[baseParts.length-2];
							String last=baseParts[baseParts.length-1];
							if (lexicon.containsKey(oneButLast)) {
								Set<String> lasts = lexicon.get(oneButLast);
								if (!lasts.contains(last)) {
									System.err.println(Arrays.toString(baseParts));
									System.err.println(" didn't find '"+last+"' in lexicon leaves generated from data: "+lasts);
								} else {
									String lexiconLabel=oneButLast+FSTNLUOutput.keyValueSep+last;
									if (ret==null) ret=new HashMap<String, Set<String>>();
									Set<String> entries=ret.get(lexiconLabel);
									if (entries==null) ret.put(lexiconLabel, entries=new HashSet<String>());
									entries.addAll(entriesForconcept.keySet());
								}
							}
						}
					}
				}
			}
		}
		return ret;
	}
	
	public static void main(String[] args) throws Exception {
		//NLUConfig config=getNLUConfig("spsFSTbackup");
		NLUConfig config=NLU.getNLUConfig("FSTNLU");
		//NLUConfig config=getNLUConfig("spsFST");
		NLBusConfig tmp = NLBusConfig.WIN_EXE_CONFIG;
		tmp.setDefaultCharacter("Base-All");
		tmp.setNluConfig(config);
		NLU nlu=NLU.init(config);
		nlu.retrain(NLUTest.ros1,NLUTest.ros2,NLUTest.ros3,NLUTest.ros5,NLUTest.ros6,NLUTest.ros7,NLUTest.ros9);
		Aligner a=new Aligner(new File(config.getNLUContentRoot()));
		List<Alignment> as = a.readAlignerOutputFile();
		AlignmentSummary asummary = new AlignmentSummary(as);
		Map<String, Map<String, Integer>> phrases = asummary.getNluConcepts2phrases();
		TDGeneralizerAndLexiconBuilder gen = new TDGeneralizerAndLexiconBuilder("(utterance_type:.*)",
				"(predicate:)([^\\:]+)",
				"(predicate_modifier:)([^\\:]+)",
				"(object:(.+:)?)([^:]+)",
				"(object_modifier:(.+:)?)([^:]+)",
				"(time:(.+:)?)([^:]+)",
				"(temporal_complement:(.+:)?)([^:]+)",
				"(location:(.+:)?)([^:]+)",
				"(source:(.+:)?)([^:]+)"
				);
		List<TrainingDataFormat> tds = gen.readTDFiles(NLUTest.ros1,NLUTest.ros2,NLUTest.ros3,NLUTest.ros5,NLUTest.ros6,NLUTest.ros7,NLUTest.ros9);
		Map<String, Set<String>> lexicon = gen.generalizeTDAndBuildLexicon(tds);
		Map<String, Set<String>> lexiconContent = gen.buildActualLexicon(phrases,lexicon);
		ExcelUtils.dumpMapToExcel((Map)lexiconContent, new File("test-lexicon-content.xlsx"), "test", null);
	}
}
