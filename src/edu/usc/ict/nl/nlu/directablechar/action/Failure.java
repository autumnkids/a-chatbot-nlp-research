package edu.usc.ict.nl.nlu.directablechar.action;

import java.util.List;

public class Failure extends Action {
	public Failure(List<String> args) {
		this.name = "FAILURE";
		this.arguments = args;
	}
	
	@Override
	public boolean good() {
		return true;
	}
}
