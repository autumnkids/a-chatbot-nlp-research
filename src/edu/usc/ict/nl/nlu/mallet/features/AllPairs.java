package edu.usc.ict.nl.nlu.mallet.features;

import cc.mallet.pipe.TokenSequenceNGrams;
import cc.mallet.types.Instance;
import cc.mallet.types.Token;
import cc.mallet.types.TokenSequence;

public class AllPairs extends TokenSequenceNGrams {

	private static final long serialVersionUID = 1;
	private static final int CURRENT_SERIAL_VERSION = 0;
	private int[] bagSizes;

	public AllPairs(int[] sizes) {
		super(sizes);
		this.bagSizes=sizes;
	}
	
	public Instance pipe (Instance carrier)
	{
		String newTerm = null;
		TokenSequence tmpTS = new TokenSequence();
		TokenSequence ts = (TokenSequence) carrier.getData();

		for (int i = 0; i < ts.size(); i++) {
			Token t = ts.get(i);
			for(int j = 0; j < bagSizes.length; j++) {
				int len = bagSizes[j];
				if (len <= 0 || len > (i+1)) continue;
				if (len == 1) { tmpTS.add(t); continue; }
				newTerm = new String(t.getText());
				for(int k = 1; k < len; k++)
					newTerm = ts.get(i-k).getText() + "_" + newTerm;
				tmpTS.add(newTerm);
			}
		}

		carrier.setData(tmpTS);

		return carrier;
	}
}
