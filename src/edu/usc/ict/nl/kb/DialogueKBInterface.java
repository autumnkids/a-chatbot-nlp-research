package edu.usc.ict.nl.kb;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import edu.usc.ict.nl.bus.modules.DM;
import edu.usc.ict.nl.dm.reward.model.DialogueOperatorEffect;

public interface DialogueKBInterface extends InformationStateInterface {
	public String getName();
		
	public Object evaluate(Object f,EvalContext context) throws Exception;
	public Object evaluate(DialogueKBFormula f,EvalContext context) throws Exception;
	public Boolean evaluate(DialogueOperatorEffect e) throws Exception;
	
	public boolean isSupportedFormulaToBeStored(DialogueOperatorEffect e);
	/**
	 * admissible values for type are:
	 *  if a new KB should be generated when a change to the KB is required, then use AUTO_NEW
	 *  else, all rules and non-trivial assertions (i.e. not converted to assignments) are always stored with access AUTO_OVERWRITETHIS
	 *        assignments and trivial assertions can be stored with access AUTO_OVERWRITETHIS or AUTO_OVERWRITEAUTO
	 * @param e
	 * @param overwrite
	 * @param doForwardInference
	 * @return
	 * @throws Exception
	 */
	public DialogueKB store(DialogueOperatorEffect e,ACCESSTYPE type,boolean doForwardInference) throws Exception; 
	public DialogueKB storeAll(Collection<DialogueOperatorEffect> effects,ACCESSTYPE type,boolean doForwardInference) throws Exception;

	public DialogueKB getParent();
	public void setParent(DialogueKB parent);
	public Collection<DialogueKB> getChildren();
	public void addChild(DialogueKB c);
	public void clearKBTree();
	
	public Collection<DialogueOperatorEffect> dumpKB() throws Exception;
	public LinkedHashMap<String, Collection<DialogueOperatorEffect>> dumpKBTree() throws Exception;
	public void printKB(String indent);
	public String getContentID() throws Exception;

	public void invalidateCache();

	public DialogueKB findFirstKBInHierarchyWithID(String name);
	public DialogueKB findThisKBInHierarchy(DialogueKB kb);
	public boolean hasVariableNamed(String vName,ACCESSTYPE type);
	public void removeVariable(String vName,ACCESSTYPE type) throws Exception;
	public Object getValueOfVariable(String vName,ACCESSTYPE type,EvalContext context);
	public DialogueKB findFirstKBInHierarchyThatContainsThisVariableName(String vName);
	public DialogueKB setValueOfVariable(String vName,Object value,ACCESSTYPE type) throws Exception;
	/**
	 *  used primarely by the SCXML dialogue manager. this uses internally a THIS_OVERWRITE access mode
	 */
	public void setValueOfVariableInKBNamed(String name,String vName,Object value) throws Exception;
	public Boolean isTrueInKB(DialogueKBFormula f,EvalContext context) throws Exception;
	public Set<String> getAllVariables() throws Exception;
	public Set<String> getAllVariablesInThisKB() throws Exception;

	/**
	 * accepts either AUTO_OVERWRITETHIS or AUTO_OVERWRITEAUTO. With THIS all modifications are stored in the current KB.
	 * @param type
	 * @throws Exception
	 */
	public void runForwardInference(ACCESSTYPE type) throws Exception;
	public LinkedHashSet<DialogueOperatorEffect> getForwardInferenceRules();
	public Boolean doesItContainThisRule(DialogueOperatorEffect e,ACCESSTYPE type);

	public DM getDM();
}
