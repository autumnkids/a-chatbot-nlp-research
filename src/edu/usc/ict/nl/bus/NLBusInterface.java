package edu.usc.ict.nl.bus;

import java.util.List;

import edu.usc.ict.nl.bus.modules.DMEventsListenerInterface;
import edu.usc.ict.nl.bus.special_variables.SpecialVar;
import edu.usc.ict.nl.nlu.NLUOutput;
import edu.usc.ict.nl.vhmsg.VHBridge.VRPerception;
import edu.usc.ict.nl.vhmsg.VHBridgewithMinat;

public interface NLBusInterface extends DMEventsListenerInterface,ExternalListenerInterface {
	public void setDialogSession2User(Long sessionID,String user);

	public List<SpecialVar> getSpecialVariables(Long sessionId) throws Exception;
	public NLUOutput getNLUOutput(Long sessionId,String userUtterance) throws Exception;
	public void setSpeakingStateVarForSessionAs(Long sessionId,Boolean state) throws Exception;
	public float getTimeUserHasBeenSpeaking();
	public void handlePerceptionEvent(VRPerception msg) throws Exception;
	public void handleMinatEvent(VHBridgewithMinat.Minat msg) throws Exception;
	public void handleLoginEvent(Long sessionId, String userID) throws Exception;

	public void addBusListener(ExternalListenerInterface i);

	public String getCharacterName4Session(Long sid);
}
