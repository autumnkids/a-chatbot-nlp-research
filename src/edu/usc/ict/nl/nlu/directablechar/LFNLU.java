package edu.usc.ict.nl.nlu.directablechar;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.usc.ict.nl.nlg.directablechar.Messanger;
import edu.usc.ict.nl.nlu.NLUOutput;
import edu.usc.ict.nl.nlu.directablechar.BuildTrainingData.LANG;
import edu.usc.ict.nl.nlu.directablechar.action.Action;
import edu.usc.ict.nl.nlu.directablechar.action.HashMessages;
import edu.usc.ict.nl.util.FileUtils;

public class LFNLU {
	private LANG lang=LANG.EN;
	private String kb=null;
	private BuildTrainingData btd=null;
	private Messanger msg=null;
	private ObjectKB objectKB;
	private Map<String, String> wordsDefs = null;
	
	public LFNLU(String url,LANG lang,String kb) throws MalformedURLException {
		btd=new BuildTrainingData(url);
		this.lang=lang;
		this.kb=kb;
		this.objectKB=new ObjectKB();
	}

	public ObjectKB getObjectKB() {
		return objectKB;
	}
	
	public NLUOutput testNLU(String u) throws Exception {
		return testNLU(u, null);
	}
	public NLUOutput testNLU(String u,StringBuffer kbContent) throws Exception {
		JSONObject r = null;
		if (kbContent!=null) {
			//use the custom kb content
			r=btd.buildJsonForRequest(kbContent,lang);
		} else {
			//use the default kb
			r=btd.buildJsonForRequest(kb,lang);
		}
		int v=btd.addUtteranceToJsonRemoveAllAlreadyThere(r, u);
		JSONObject result=btd.sendRequest(r);
		System.out.println("---------------------Result--------------------------");
		System.out.println(result.toString());
		System.out.println("-----------------------------------------------------");
		if (result!=null && result.has(BuildTrainingData.utterancesKey)) {
			JSONArray utterances=BuildTrainingData.getUtterances(result);
			if (utterances!=null) {
				int max=utterances.length();
				for(int i=0;i<max;i++) {
					JSONObject uo=utterances.getJSONObject(i);
					//System.out.println(uo);
					if (uo.has(BuildTrainingData.abductionKey)) {
						List<Literal> lf = BuildTrainingData.extractParsingPart(uo);
						// Here to record the definitions of some words
						RecordWordsDefinitions(lf);
						NLUOutput inter=generateInterpretation(lf,u);
						if (inter!=null) return inter;
					}
				}
			}
		}
		return null;
	}
	
	private void RecordWordsDefinitions(List<Literal> lf) {
		if (wordsDefs == null) wordsDefs = new HashMap<String, String>();
		if (!wordsDefs.isEmpty()) wordsDefs.clear();
		for (Literal l:lf) {
			// Tokenize the axioms
			String[] tokens = l.toString().split("[(,)]");
			for (String t:tokens) {
				System.out.println("---------------------Tokens------------------------");
				System.out.println(t);
				System.out.println("-----------------------------------------------------");
			}
			// If it looks like a definition, put it into dictionary
			if (tokens.length == 3 && tokens[2].charAt(0) == 'x' && tokens[1].charAt(0) == 'e') {
				String key = tokens[2];
				String value = "";
				if (wordsDefs.get(key) != null) {
					value = wordsDefs.get(key);
					String[] words = tokens[0].split("-");
					value += " " + words[0];
				} else {
					String[] words = tokens[0].split("-");
					value = words[0];
				}
				System.out.println("---------------------WordsDefs-----------------------");
				System.out.println("key: " + key + " obj: " + value);
				System.out.println("-----------------------------------------------------");
				wordsDefs.put(key, value);
			}
		}
	}
	
	private NLUOutput generateInterpretation(List<Literal> lf, String utterance) throws Exception {
		List<Literal> inference=getInferredParts(lf);
		List<Action> as=getAction(inference);
		if (as!=null) {
			for(Action a:as) {
				if (a!=null) {
					System.out.println("---------------------Generate------------------------");
					System.out.println(utterance + " - " + a.getName());
					System.out.println("-----------------------------------------------------");
					return new NLUOutput(utterance, a.getName(), 1f, a.getPayload());
				}
			}
		}
		return null;
	}

	private List<Action> getAction(List<Literal> lf) throws Exception {
		List<Action> ret=null;
		if(lf!=null && !lf.isEmpty()) {
			for(Literal l:lf) {
				if (l.getP().equals("ACTION")) {
					List<String> args=l.getArgs();
					for (int i = 0; i < args.size(); i++) {
						System.out.println("-------------------ACTIONS---------------------------");
						System.out.println(args.get(i));
						System.out.println("-----------------------------------------------------");
					}
					if (args==null || args.isEmpty() || args.size()<1) throw new Exception("invalid number of arguments to action");
					else {
						String name=args.get(0);
						System.out.println("---------- " + name);
						// If it is FAILUE, then deal with the arguments (like: x1, x2, ...)
						if (name.equals("FAILURE")) {
							if (args.get(1).equals("PROPERTY")) {
								String arg = wordsDefs.get(args.get(3));
								args.set(3, arg);
								HashMessages.property = arg;
								System.out.println("---------- " + args.get(3));
							} else if (args.get(1).equals("WHO")) {
								String arg = wordsDefs.get(args.get(4));
								args.set(4, arg);
								HashMessages.who = arg;
								System.out.println("---------- " + args.get(4));
							} else if (args.get(1).equals("BOTH")) {
								String arg = wordsDefs.get(args.get(3));
								args.set(3, arg);
								HashMessages.property = arg;
								System.out.println("---------- " + args.get(3));
								arg = wordsDefs.get(args.get(4));
								args.set(4, arg);
								HashMessages.who = arg;
								System.out.println("---------- " + args.get(4));
							}
						} else if (name.equals("QUERY")) {
							/*
							String arg = wordsDefs.get(args.get(4));
							args.set(4, arg);
							HashMessages.property = arg;
							arg = wordsDefs.get(args.get(5));
							args.set(5, arg);
							HashMessages.who = arg;
							*/
							String arg = args.get(2).toLowerCase();
							HashMessages.property = arg.replaceAll("_", " ");
							arg = args.get(3).toLowerCase();
							HashMessages.who = arg;
						}
						Action a=Action.create(name,args.subList(1,args.size()));
						if (a != null) System.out.println("---------- a != null");
						if (a.good()) System.out.println("---------- a is good");
						if (a!=null && a.good()) {
							if (ret==null) ret=new ArrayList<Action>();
							ret.add(a);
						}
					}
				}
			}
		}
		if (ret!=null) Collections.sort(ret);
		return ret;
	}

	private List<Literal> getInferredParts(List<Literal> lf) {
		List<Literal> ret=null;
		if(lf!=null && !lf.isEmpty()) {
			for(Literal l:lf) {
				if (Character.isUpperCase(l.getP().charAt(0))) {
					if (ret==null) ret=new ArrayList<Literal>();
					ret.add(l);
				}
			}
		}
		return ret;
	}

	public void kill() {
		if (msg!=null) msg.kill();
	}
	
	public static void main(String[] args) throws Exception {
		LFNLU nlu = new LFNLU("http://colo-vm19.isi.edu:8081/",LANG.EN,"KBs/kb.da");
		StringBuffer kb=FileUtils.readFromFile("resources/characters/Directable/nlu/kb.txt");
		NLUOutput r = nlu.testNLU("create a red ball",kb);
		System.out.println(r);
	}
}
