package edu.usc.ict.nl.kb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.usc.ict.nl.bus.modules.DM;
import edu.usc.ict.nl.dm.reward.model.DialogueOperatorEffect;
import edu.usc.ict.nl.util.graph.Node;

public abstract class DialogueKB extends Node implements DialogueKBInterface {

	protected DM dm=null;
	protected Set<String> tracedConstants=null;

	public DialogueKB(DM dm) {
		this.dm=dm;
	}
	
	@Override
	public DM getDM() {
		return dm;
	}
	
	@Override
	public DialogueKB findFirstKBInHierarchyWithID(String name) {
		String thisName=getName();
		if (thisName.equalsIgnoreCase(name)) return this;
		else {
			DialogueKBInterface parent=null;
			if ((parent=getParent())!=null) return parent.findFirstKBInHierarchyWithID(name);
		}
		return null;
	}
	@Override
	public DialogueKB findThisKBInHierarchy(DialogueKB kb) {
		if (this.equals(kb)) return this;
		else {
			DialogueKBInterface parent=null;
			if ((parent=getParent())!=null) return parent.findThisKBInHierarchy(kb);
		}
		return null;
	}
	public static boolean isOverwriteMode(ACCESSTYPE type) {
		switch (type) {
		case AUTO_OVERWRITEAUTO:
		case AUTO_OVERWRITETHIS:
		case THIS_OVERWRITETHIS:
			return true;
		default:
			return false;
		}
	}
	
	public void addTracingFor(String n) {
		if (tracedConstants==null) tracedConstants=new HashSet<String>();
		tracedConstants.add(n);
	}
	public void removeTracingFor(String n) {
		if (tracedConstants!=null) tracedConstants.remove(n);
	}
	public boolean getTracing(String n) {
		if (tracedConstants!=null) return tracedConstants.contains(n);
		else return false;
	}

	public Collection<Change> saveAssignmentsAndGetUpdates(ACCESSTYPE type, boolean doForwardInference, DialogueOperatorEffect... effs) throws Exception {
		return saveAssignmentsAndGetUpdates(type, doForwardInference, Arrays.asList(effs));
	}
	public Collection<Change> saveAssignmentsAndGetUpdates(ACCESSTYPE type, boolean doForwardInference, List<DialogueOperatorEffect> effs) throws Exception {
		effs=processAssignmentListToRemoveImplications(effs);
		Collection<Change> ret=null;
		DialogueKBInterface tmpKB = storeAll(effs, ACCESSTYPE.AUTO_NEW, doForwardInference);
		if (tmpKB!=null) {
			Collection<DialogueOperatorEffect> changes = tmpKB.dumpKB();
			if (changes!=null) {
				for(DialogueOperatorEffect e:changes) {
					DialogueKBFormula var=e.getAssignedVariable();
					if (var!=null) {
						String varName=var.getName();
						Object newValue=e.getAssignedExpression();
						Object oldValue=getValueOfVariable(varName,ACCESSTYPE.AUTO_OVERWRITEAUTO,null);
						if (ret==null) ret=new ArrayList<Change>();
						ret.add(new Change(varName, oldValue, newValue));
						logger.info("variable "+varName+" changed from "+oldValue+" to "+newValue);
						// updating information state.
						// keep track of local variables
						store(e, type, false);
						/*if (localVars!=null && (localVars==localVars.findFirstKBInHierarchyThatContainsThisVariableName(varName))) {
							logger.info("NODE EXE: saving '"+varName+"' in local kb.");
							localVars.store(e, ACCESSTYPE.AUTO_OVERWRITEAUTO, false);
						} else {
							permanentInfoState.store(e, ACCESSTYPE.AUTO_OVERWRITEAUTO, false);
							logger.info("NODE EXE: saving '"+varName+"' in global kb.");
						}*/

					} else logger.error("Error in change effect: "+e);
				}
			}
		}
		return ret;
	}

	public List<DialogueOperatorEffect> processAssignmentListToRemoveImplications(List<DialogueOperatorEffect> effs) {
		List<DialogueOperatorEffect> ret=null;
		if (effs!=null) {
			for(DialogueOperatorEffect e:effs) {
				DialogueOperatorEffect newe=null;
				if (e.isImplication()) {
					DialogueKBFormula ante=e.getAntecedent();
					DialogueOperatorEffect conse=e.getConsequent();
					DialogueOperatorEffect elsePart=e.getElseConsequent();
					try {
					Boolean result=(Boolean) evaluate(ante,null);
					if (result!=null) {
						if (result)	newe=conse;
						else newe=elsePart;
					}
					} catch (Exception ex) {logger.error(ex);}
				} else {
					newe=e;
				}
				if (newe!=null) {
					if (ret==null) ret=new ArrayList<DialogueOperatorEffect>();
					ret.add(newe);
				}
			}
		}
		return ret;
	}
	
	public Collection<Change> getCurrentValues(Set<String> excludeTheseVars) throws Exception {
		Collection<DialogueOperatorEffect> effs = dumpKB();
		Collection<Change> ret=null;
		if (effs!=null) {
			for(DialogueOperatorEffect e:effs) {
				DialogueKBFormula v = e.getAssignedVariable();
				String name=v.getName();
				if (excludeTheseVars==null || !excludeTheseVars.contains(name)) {
					if (ret==null) ret=new ArrayList<Change>();
					Object newValue=e.getAssignedExpression();
					ret.add(new Change(name, null, newValue));
				}
			}
		}
		return ret;
	}
}
