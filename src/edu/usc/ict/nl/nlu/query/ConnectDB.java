package edu.usc.ict.nl.nlu.query;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDB {
	private Connection connect = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	
	private static String DBNAME = "KnowledgeBase";
	private static String DEFAULTTABLENAME = "Refs";
	
	public String Connect(String property, String constraint, String proText, String conText) throws Exception {
		String result = "";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost/" + DBNAME + "?"
					+ "user=root&password=");
			statement = connect.createStatement();
			result = readData(property, constraint, proText, conText);
		} catch (Exception e) {
			System.out.println("-------------------Database--------------------------");
			System.out.println("Cannot connect db");
			System.out.println("-----------------------------------------------------");
			throw e;
		} finally {
			Close();
		}
		return result;
	}
	
	public String readData(String property, String constraint, String proText, String conText) {
			try {
				String query = 	"select TableName, TableID from " + DBNAME + "." + DEFAULTTABLENAME + 
								" where Name='" + constraint + "'";
				resultSet = statement.executeQuery(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		try {
			if (resultSet.next()) {
				String tableName = resultSet.getString("TableName");
				int tableID = resultSet.getInt("TableID");
				String query = "";
				if (property.equals("RELIGIOUS_BELIEF")) {
					query = "select ReligiousBelief from " + DBNAME + "." + tableName + 
							" where id=" + String.valueOf(tableID);
					System.out.println("-------------------Database--------------------------");
					System.out.println(query);
					System.out.println("-----------------------------------------------------");
				}
				ResultSet subResult = statement.executeQuery(query);
				if (subResult.next()) {
					if (property.equals("RELIGIOUS_BELIEF")) {
						if (!subResult.getString("ReligiousBelief").equals("NULL")) {
							return "It's " + subResult.getString("ReligiousBelief") + ".";
						} else {
							// Send event
						}
					} else if (property.equals("BIRTH_DATE")) {
						//
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Sorry, it seems " + conText + " doesn't have " + proText + ".";
	}
	
	public void Close() throws SQLException {
		if (connect != null) connect.close();
		if (statement != null) statement.close();
		if (resultSet != null) resultSet.close();
	}
}
