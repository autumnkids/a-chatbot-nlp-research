package edu.usc.ict.nl.bus.events;



public class SystemUtteranceInterruptedEvent extends SystemUtteranceDoneEvent {

	public SystemUtteranceInterruptedEvent(String name, Long sid) {
		super(name, sid);
	}
}
