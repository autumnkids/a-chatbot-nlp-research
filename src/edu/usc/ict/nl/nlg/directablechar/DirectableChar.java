package edu.usc.ict.nl.nlg.directablechar;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import com.oracle.webservices.internal.api.message.PropertySet.Property;

import edu.usc.ict.nl.bus.NLBusInterface;
import edu.usc.ict.nl.bus.events.DMSpeakEvent;
import edu.usc.ict.nl.bus.events.NLGEvent;
import edu.usc.ict.nl.bus.modules.NLUInterface;
import edu.usc.ict.nl.config.NLBusConfig;
import edu.usc.ict.nl.kb.DialogueKBFormula;
import edu.usc.ict.nl.kb.DialogueKBInterface;
import edu.usc.ict.nl.nlg.echo.EchoNLG;
import edu.usc.ict.nl.nlu.directablechar.LFNLU2;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB.COLOR;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB.DCObject;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB.FAILURE_TYPE;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB.PROPERTY;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB.QTYPE;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB.SHAPE;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB.SIZE;
import edu.usc.ict.nl.nlu.directablechar.ObjectKB.WHO;
import edu.usc.ict.nl.nlu.directablechar.ReplyMessageProcessor;
import edu.usc.ict.nl.nlu.directablechar.action.HashMessages;
import edu.usc.ict.nl.nlu.query.ConnectDB;
import edu.usc.ict.nl.util.StringUtils;
import edu.usc.ict.vhmsg.MessageEvent;

public class DirectableChar extends EchoNLG {

	private Messanger msg=null;
	private static final String base="object";
	private static int objectCounter=1;

	public DirectableChar(NLBusConfig c) {
		super(c);
		this.msg=new Messanger();
	}
	
	@Override
	public NLGEvent doNLG(Long sessionID, DMSpeakEvent ev, boolean simulate)
			throws Exception {
		//return super.doNLG(sessionID, ev, simulate);
		NLBusInterface m = getNLModule();
		NLUInterface nlu = m.getNlu(sessionID);
		boolean pick=false;
		if (nlu!=null && nlu instanceof LFNLU2) {
			String name=ev.getName();
			if (name.equals("RCREATE")) {
				ObjectKB objectKB = ((LFNLU2)nlu).getObjectKB();
				DialogueKBInterface is = ev.getLocalInformationState();
				if (is!=null) {
					SHAPE shape = getValueOfEnum(is,"SHAPE", SHAPE.class);
					SIZE size = getValueOfEnum(is,"SIZE", SIZE.class);
					COLOR color = getValueOfEnum(is,"COLOR", COLOR.class);
					DCObject ob = createObject(objectKB,null, color, shape, size, null);
					return new NLGEvent(name+"("+ob+")", sessionID, ev);
				}
			} else if ((pick=name.equals("PICK")) || name.equals("GOTO")) {
				DialogueKBInterface is = ev.getLocalInformationState();
				if (is!=null) {
					String objectName=null;
					Object ov=is.get("ARG0");
					if (ov!=null) {
						ov=is.evaluate(ov, null);
						if (ov!=null && ov instanceof String) {
							objectName=DialogueKBFormula.getStringValue((String)ov);
						}
					}
					if (!StringUtils.isEmptyString(objectName)) {
						boolean found=gotoObject(objectName,pick);
						if (found) {
							return new NLGEvent(name+"("+objectName+")", sessionID, ev);
						} else {
							return new NLGEvent("'"+objectName+"' not found", sessionID, ev);
						}
					}
				}
			} else if (name.equals("DELETE")) {
				DialogueKBInterface is = ev.getLocalInformationState();
				if (is!=null) {
					String objectName=null;
					Object ov=is.get("ARG0");
					if (ov!=null) {
						ov=is.evaluate(ov, null);
						if (ov!=null && ov instanceof String) {
							objectName=DialogueKBFormula.getStringValue((String)ov);
						}
					}
					if (!StringUtils.isEmptyString(objectName)) {
						boolean found=deleteObject(objectName);
						if (found) {
							return new NLGEvent(name+"("+objectName+")", sessionID, ev);
						} else {
							return new NLGEvent("'"+objectName+"' not found", sessionID, ev);
						}
					}
				}
			} else if (name.equals("QUERY")) {
				DialogueKBInterface is = ev.getLocalInformationState();
				if (is != null) {
					QTYPE qtype = getValueOfEnum(is, "QTYPE", QTYPE.class);
					PROPERTY property = getValueOfEnum(is, "PROPERTY", PROPERTY.class);
					WHO who = getValueOfEnum(is, "WHO", WHO.class);
					ConnectDB cdb = new ConnectDB();
					String result = cdb.Connect(property.name(), who.name(), HashMessages.property, HashMessages.who);
					System.out.println("--------------------Result---------------------------");
					System.out.println(result);
					System.out.println("-----------------------------------------------------");
					return new NLGEvent(result, sessionID, ev);
				}
				return new NLGEvent("QUERY", sessionID, ev);
			} else if (name.equals("FAILURE")) {
				DialogueKBInterface is = ev.getLocalInformationState();
				if (is != null) {
					FAILURE_TYPE fail = getValueOfEnum(is, "FAILURE_TYPE", FAILURE_TYPE.class);
					QTYPE qtype = getValueOfEnum(is, "QTYPE", QTYPE.class);
					String propertyStr = "";
					String whoStr = "";
					if (fail.name().equals("PROPERTY")) {
						propertyStr = "'" + HashMessages.property + "'";
						WHO who = getValueOfEnum(is, "WHO", WHO.class);
						whoStr = who.name();
					} else if (fail.name().equals("WHO")) {
						PROPERTY property = getValueOfEnum(is, "PROPERTY", PROPERTY.class);
						propertyStr = property.name();
						whoStr = "'" + HashMessages.who + "'";
					} else if (fail.name().equals("BOTH")) {
						propertyStr = "'" + HashMessages.property + "'";
						whoStr = "'" + HashMessages.who + "'";
					}
					System.out.println("--------------------Result---------------------------");
					System.out.println("FAILURE-" + fail.name() + "(" + qtype.name() + "," + propertyStr + "," + whoStr + ")");
					System.out.println("-----------------------------------------------------");
					String result = "FAILURE-" + fail.name() + "(" + qtype.name() + "," + propertyStr + "," + whoStr + ")";
					// Generate sentence
					if (fail.name().equals("PROPERTY")) {
						result = "Sorry, I don't know what is '" + propertyStr + "'";
					} else if (fail.name().equals("WHO")) {
						result = "Sorry, I don't know who is '" + whoStr + "'";
					} else if (fail.name().equals("BOTH")) {
						result = "Sorry, I don't know what is '" + propertyStr + "', and who is '" + whoStr + "'";
					}
					return new NLGEvent(result, sessionID, ev);
				}
				return new NLGEvent("FAILURE", sessionID, ev);
			} else {
				return super.doNLG(sessionID, ev, simulate);
			}
		}
		return null;
	}
	public <E extends Enum<E>> E getValueOfEnum(DialogueKBInterface is,String name,Class<E> enumType) {
		try {
			Object ov=is.get(name);
			if (ov!=null) {
				ov=is.evaluate(ov, null);
				if (ov!=null && ov instanceof String) {
					return Enum.valueOf(enumType,DialogueKBFormula.getStringValue((String)ov));
				}
			}
		} catch (Exception ex) {}
		return null;
	}
	
	public DCObject createObject(ObjectKB objectKB, String name,ObjectKB.COLOR color,ObjectKB.SHAPE shape,ObjectKB.SIZE sizeMod,float[] xyz) throws Exception {
		Set<String> obs=getListOfObjectsInWorld();
		
		// get the name
		if (StringUtils.isEmptyString(name)) name=base;
		while(obs.contains(name)) {
			name=base+objectCounter;
			objectCounter++;
		}
			
		//complete other properties
		DCObject obj = objectKB.completeProperties(shape, sizeMod, color);
		obj.setProperty(name);
		
		shape=obj.getShape();
		sizeMod=obj.getSize();
		color=obj.getColor();
		Color awtColor;
		try {
		    Field field = Color.class.getField(color.toString());
		    awtColor = (Color)field.get(null);
		} catch (Exception e) {
			awtColor = null;
		}
		
		float[] s=ObjectKB.getSizeForShape(shape,sizeMod);
		Random rg=new Random();
		//position
		if (xyz==null) {
			xyz=new float[3];
			xyz[0]=(rg.nextFloat()-0.5f)*8f;
			xyz[2]=(rg.nextFloat()-0.5f)*8f;
			xyz[1]=0+s[1];
		}
		
		//get the color
		int r=awtColor.getRed()/255;
		int g=awtColor.getGreen()/255;
		int b=awtColor.getBlue()/255;
		final String id = UUID.randomUUID().toString();
		msg.waitForReply(id, "sb", "myobject = scene.createPawn(\""+name+"\");myobject.setStringAttribute(\"collisionShape\", \""+shape.toString().toLowerCase()+"\");myobject.setVec3Attribute(\"collisionShapeScale\", "+s[0]+", "+s[1]+", "+s[2]+");myobject.setVec3Attribute(\"color\", "+r+", "+g+", "+b+");myobject.setPosition(SrVec("+xyz[0]+", "+xyz[1]+", "+xyz[2]+"))", null, 10);
		obs=getListOfObjectsInWorld();
		if (obs.contains(name)) objectKB.storeObject(obj);  
		return obj;
	}

	public Set<String> getListOfObjectsInWorld() throws InterruptedException {
		final Set<String> ret=new HashSet<String>();
		final String id = UUID.randomUUID().toString();
		ReplyMessageProcessor replyListener = new ReplyMessageProcessor() {
			@Override
			public void processMessage(MessageEvent e) {
				Map<String, ?> map = e.getMap();
				if(map.containsKey(id)){
					String msg=(String) map.get(id);
					if (!StringUtils.isEmptyString(msg)) {
						String[] objects=msg.split("\t");
						ret.addAll(Arrays.asList(objects));
					}
				}
			}
		};
		msg.waitForReply(id, "sb","objects = scene.getPawnNames();str = '\t'.join(objects);scene.vhmsg2(\""+id+"\", str)",replyListener,3);
		return ret;
	}
	
	public boolean gotoObject(String name,boolean pick) throws InterruptedException {
		name=name.toLowerCase();
		Set<String> obs = getListOfObjectsInWorld();
		if (obs.contains(name)) {
			final String id = UUID.randomUUID().toString();
			if (pick) {
				msg.waitForReply(id, "sb", "bml.execBML('ChrBrad', '<sbm:reach sbm:action=\"pick-up\" sbm:reach-duration=\"0.2\" sbm:use-locomotion=\"true\" target=\""+name+"\"/>')", null, 10);
			} else {
				msg.waitForReply(id, "sb", "bml.execBML('ChrBrad', '<locomotion target=\""+name+"\"/>')", null, 10);
			}
			return true;
		}
		return false;
	}
	public boolean deleteObject(String name) throws InterruptedException {
		name=name.toLowerCase();
		Set<String> obs = getListOfObjectsInWorld();
		if (obs.contains(name)) {
			final String id = UUID.randomUUID().toString();
			msg.waitForReply(id, "sb", "scene.removePawn(\""+name+"\")", null, 10);
			return true;
		}
		return false;
	}
	
	/*
	 * ~ c = scene.getCharacter("ChrBrad")

~ pos = c.getPosition()
~ print pos.getData(0)
-0.174194857478

~ print pos.getData(2)
0.171502828598

reach after locomotion


   pawn released: object


	 */


}
