package edu.usc.ict.nl.dm.reward.trackers;

import edu.usc.ict.nl.bus.NLBus;
import edu.usc.ict.nl.dm.reward.RewardDM;
import edu.usc.ict.nl.kb.DialogueKB;
import edu.usc.ict.nl.kb.InformationStateInterface.ACCESSTYPE;


public class UserFinishedSpeakingTracker extends ValueTracker {
	public UserFinishedSpeakingTracker(RewardDM dm) {
		super(dm);
	}

	@Override
	public Boolean getter() {
		DialogueKB is = dm.getInformationState();
		Boolean userSpeaking=null;
		try {
			userSpeaking = (Boolean) is.evaluate(is.getValueOfVariable(NLBus.userSpeakingStateVarName.getName(),ACCESSTYPE.AUTO_OVERWRITEAUTO,null),null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		boolean ret=userSpeaking==null || !userSpeaking;
		//call the setter to make sure that other methods depending on the set values are in sinck with the result provided by this getter call.
		setter(ret);
		return ret;
	}

	@Override
	protected void setter(Object cv) {
		boolean currentValue=(Boolean)cv;
		Boolean oldValue=(Boolean)value;
		if (oldValue==null || currentValue!=oldValue) {
			value=currentValue;
			if (currentValue) touch();
		}
		if (!currentValue) {// i.e. the user is speaking, update the length variable.
			DialogueKB is = dm.getInformationState();
			try {
				float time = dm.getMessageBus().getTimeUserHasBeenSpeaking();
				if (time>0) is.setValueOfVariable(NLBus.lengthOfLastThingUserSaidVarName.getName(),time,ACCESSTYPE.AUTO_OVERWRITEAUTO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
