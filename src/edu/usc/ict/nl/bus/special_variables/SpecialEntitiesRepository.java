package edu.usc.ict.nl.bus.special_variables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

public class SpecialEntitiesRepository {
	private final LinkedHashMap<String, SpecialVar> svs=new LinkedHashMap<String, SpecialVar>();
	public void addSpecialVariable(SpecialVar sv) {svs.put(sv.getName(), sv);}
	public SpecialVar get(String name) {return svs.get(name.toLowerCase());}
	public Collection<SpecialVar> getVisibleVars() {
		Collection<SpecialVar> ret=null;
		for(SpecialVar sv:svs.values()) {
			if (!sv.isHidden()) {
				if (ret==null) ret=new ArrayList<SpecialVar>();
				ret.add(sv);
			}
		}
		return ret;
	}
	public Set<SpecialVar> getAllSpecialVariables() {
		return (svs!=null)?new HashSet<SpecialVar>(svs.values()):null;
	}
}
